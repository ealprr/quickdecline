# QuickDecline

Quick Decline is an android app that let you quickly send pre-formatted sms when declining call.


## Screenshots

First, decline a call and then, select a quick answer to send:

![Decline call](https://bitbucket.org/ealprr/quickdecline/wiki/00-decline-call.png)
![Select a quick answer](https://bitbucket.org/ealprr/quickdecline/wiki/01-quick-answer.png)


### Configuration

![Configure QuickDecline](https://bitbucket.org/ealprr/quickdecline/wiki/04-config.png)
![Edit sms message](https://bitbucket.org/ealprr/quickdecline/wiki/05-edit-sms.png)


## Installation

Get the application here: [QuickDecline v0.2](https://bitbucket.org/ealprr/quickdecline/downloads/QuickDecline-02.apk).


## Changelog

Release note : 

 - **1.2** - 2014-05-16
     - translation into arabic langage by Abdellatif Hssaini
 - **1.1** - 08/06/2012
     - addded the possibility to edit your message within bulk sms app before sending it
 - **1.0** - 10/07/2011
     - first version


## Roadmap

 * publish source code...
 * change app icon
 * add other langages
 * publish on android market


## Licence

GPL3
